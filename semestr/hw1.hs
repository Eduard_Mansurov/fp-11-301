module Main where

import  Data.List

data Expr = Var String
          | App Expr Expr
          | Lam String Expr
          deriving (Show)

freeVars :: Expr -> [String]
freeVars (Var s) = [s]
freeVars (App a b) = freeVars a `union` freeVars b
freeVars (Lam c d) = freeVars d \\ [c]

boundVariables :: Expr -> [String]
boundVariables (Var _) = []
boundVariables (App term1 term2) = (boundVariables term1) `union` (boundVariables term2)
boundVariables (Lam name term) = name : (boundVariables term)

alphaConversion :: String -> Expr -> Expr -> Expr
alphaConversion v x b = alphaConversion' b
  where alphaConversion' (Var i) = if i == v then x else Var i
        alphaConversion' (App f a) = App (alphaConversion' f) (alphaConversion' a)
        alphaConversion' (Lam i e) =
            if v == i then
                Lam i e
            else if i `elem` vars || i `elem` boundVars then
                let i' = checkIn e i
                    e' = alphaConversion i (Var i') e
                in  Lam i' (alphaConversion' e')
            else
				Lam i (alphaConversion' e)
        vars  = freeVars x
        boundVars = boundVariables x
        checkIn e i = helper i
           where helper i' = if i' `elem` vars' then helper (i ++ "'") else i'
                 vars' = vars ++ boundVars

betaReduction :: Expr -> Expr
betaReduction ee = helper ee []
  where helper (App f a) as = helper f (a:as)
        helper (Lam s e) [] = Lam s (betaReduction e)
        helper (Lam s e) (a:as) = helper (alphaConversion s a e) as
        helper f as = app f as
        app f as = foldl App f (map betaReduction as)